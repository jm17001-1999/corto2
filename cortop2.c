#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//struct de puntos
struct Point {
    int x, y;
};


//Obtener la distancia por medio de la formula
double getDistancia(struct Point a, struct Point b)
{
    double distancia;
    distancia = sqrt((a.x - b.x) * (a.x - b.x) + (a.y-b.y) *(a.y-b.y));
    return distancia;
}




int main()
{  double pendiente;
    struct Point a, b;
    printf("Ingrese la cordenada del punto a: ");
    scanf("%d %d", &a.x, &a.y);
    printf("Ingrese la cordenada del punto b: ");
    scanf("%d %d", &b.x, &b.y);
   //Calculamos la pendiente con la formula
     pendiente = (b.y - a.y)/(b.x - a.x);
    printf("la distancia entre los puntos a y b: %lf\n", getDistancia(a, b));
  
    
   
    
    return printf("La ecuaciion de la recta es: y-%d = %d (x - %d)  \n ", a.y , pendiente , a.x);

    return 0;
}
